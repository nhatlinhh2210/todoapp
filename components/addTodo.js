import React, { useState } from "react";
import { StyleSheet, Text, TextInput, Button, View } from "react-native";

// Them ham submitHandler o App.js de bat su kien add.
export default function AddTodo({ submitHandler }) {
  const [text, setText] = useState("");

  // tao function bat su kien thay doi text
  const changeHandler = (val) => {
    setText(val);
  };

  return (
    <View>
      <TextInput
        style={styles.input}
        placeholder="New todo..."
        onChangeText={changeHandler}
      />
      {/* Tao button them viec can lam */}
      {/* onPress goi toi ham submitHandler voi du lieu text duoc nhap vao */}
      <Button onPress={() => submitHandler(text)} title="Add" color="coral" />
    </View>
  );
}

const styles = StyleSheet.create({
  input: {
    marginBottom: 10,
    paddingHorizontal: 8,
    paddingVertical: 6,
    borderBottomWidth: 1,
    borderBottomColor: "#ddd",
  },
});
