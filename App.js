import { StatusBar } from "expo-status-bar";
import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  Alert,
  //  Ham bat su kien click
  TouchableWithoutFeedback,
  Keyboard,
} from "react-native";
import Header from "./components/header";
import TodoItem from "./components/totoitem";
import AddTodo from "./components/addTodo";
import Sandbox from "./components/sandbox";

export default function App() {
  // Tao bien chua data viec can lam
  const [todos, setTodos] = useState([
    { text: "Wake up at 6 AM", key: "1" },
    { text: "Breakfast at 7 AM", key: "2" },
    { text: "Do some homework", key: "3" },
    { text: "Go to school", key: "4" },
  ]);

  // Ham nhan su kien click vao item
  const pressHandler = (key) => {
    setTodos((prevTodos) => {
      return prevTodos.filter((todo) => todo.key != key);
    });
  };

  // Ham nhan su kien add
  // Them bien text dem tu component addTodo,
  // goi ham setTodos de them vao mang,
  // tra ve mang moi gom text nhan tu addTodo va item co san trong mang
  const submitHandler = (text) => {
    // check du lieu nhap vao
    // Neu tren 3 ky tu thi thuc thi
    if (text.length > 3) {
      setTodos((prevTodos) => {
        return [{ text: text, key: Math.random().toString() }, ...prevTodos];
      });
    }
    // Khi khong <= 3 ky tu thi hien pop up canh bao
    // Khi tat canh bao thi ghi hanh dong vao console log
    else {
      Alert.alert("Phai nhap tren 3 ky tu!", "Leu Leu", [
        { text: "Okela", onPress: () => console.log("Da tat canh bao") },
      ]);
    }
  };

  return (
    //<Sandbox />
    // Goi ham bat su kien click man hinh o bat ky vi tri nao
    // Khi nhan su kien thi se ha ban phim xuong
    <TouchableWithoutFeedback
      onPress={() => {
        Keyboard.dismiss();
        console.log("dismissed keyboard");
      }}
    >
      <View style={styles.container}>
        {/* Header */}
        <Header />

        {/* Body */}
        <View style={styles.content}>
          {/* Form them viec can lam */}
          {/* Goi ham submitHandler de nhan su kien tu addTodo */}
          <AddTodo submitHandler={submitHandler} />

          {/* form can lam */}
          <View style={styles.list}>
            {/* Dung FlatList de hien thi task can lam */}
            <FlatList
              data={todos}
              renderItem={({ item }) => (
                // <Text>{item.text}</Text>
                <TodoItem item={item} pressHandler={pressHandler} />
              )}
            />
          </View>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  content: {
    padding: 40,
    flex: 1,
  },
  list: {
    // them flex 1 de list kh chay lo' khoi khung man hinh
    flex: 1,
    marginTop: 20,
  },
});
